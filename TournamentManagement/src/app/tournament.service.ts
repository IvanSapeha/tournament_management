import {Injectable, Inject} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import { Tournament } from './tournament';
import { APP_BASE_HREF } from '@angular/common'; 
 
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable()
export class TournamentService {
    constructor(private http:HttpClient, @Inject(APP_BASE_HREF) private baseHref:string) {
    }
 
    // Uses http.get() to load data from a single API endpoint
    getTournaments() {
      console.warn('in get');
        return this.http.get<Tournament[]>(this.baseHref +'api/Tournament/GetTournaments');
    }
}