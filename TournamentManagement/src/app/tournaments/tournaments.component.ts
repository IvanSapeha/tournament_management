import { Component, OnInit, } from '@angular/core';
import {Tournament} from '../tournament';
import { TournamentService } from '../tournament.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-tournaments',
  templateUrl: './tournaments.component.html',
  styleUrls: ['./tournaments.component.css']
})
export class TournamentsComponent implements OnInit {

  private tournamnets: Observable<Tournament[]>;
  
  constructor(private tournamentService: TournamentService) {  
  }

  ngOnInit(){
    this.tournamnets = this.tournamentService.getTournaments();
  }

}
