using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TournamentManagement.Models
{
  public class Team
  {
    public int Id { get; set; }
    public string Title { get; set; }
    public string Website { get; set; }
    public List<Player> Players { get; set; }
  }
}
