using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TournamentManagement.Models
{
  public class Tournament
  {
    public int Id { get; set; }
    public string Title { get; set; }
    public List<Team> Teams { get; set; }
  }
}
