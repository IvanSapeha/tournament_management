using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using TournamentManagement.EntityFrameworkContext;
using Microsoft.AspNetCore.SpaServices.AngularCli;

namespace TournamentManagement
{
    public class Startup
    {
        private readonly IHostingEnvironment _hostingEnv;

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;

            _hostingEnv = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            var physicalProvider = _hostingEnv.WebRootFileProvider;
            services.AddSingleton<IFileProvider>(physicalProvider);

            services.AddDbContext<TournamentManagementContext>(options =>
              options.UseSqlServer(Configuration.GetConnectionString("TournamentManagementDatabase")));

            services.AddSpaStaticFiles(configuration =>
            {
              configuration.RootPath = "wwwroot/dist";
            });
    }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseMvc(routes => routes.MapRoute("SpaFallback", "{*all}", new { controller = "Home", action = "Index" }));

            app.UseSpa(spa =>
            {
              // To learn more about options for serving an Angular SPA from ASP.NET Core,
              // see https://go.microsoft.com/fwlink/?linkid=864501

              spa.Options.SourcePath = "wwwroot";

              if (env.IsDevelopment())
              {
                spa.UseAngularCliServer(npmScript: "start");
              }
            });
         }
    }
}
