using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TournamentManagement.Models;

namespace TournamentManagement.EntityFrameworkContext
{
  public class TournamentManagementContext : DbContext
  {
    public TournamentManagementContext()
    {
      Database.EnsureCreated();

      if (!Tournaments.Any())
      {
        Tournaments.Add(new Tournament { Title = "Tournament1" });
        Tournaments.Add(new Tournament { Title = "Tournament2" });
        Tournaments.Add(new Tournament { Title = "Tournament3" });
        SaveChanges();
      }
    }

    public TournamentManagementContext(DbContextOptions<TournamentManagementContext> options)
    : base(options)
    { }
    public DbSet<Tournament> Tournaments { get; set; }
    public DbSet<Team> Teams { get; set; }
    public DbSet<Player> Players { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      if (!optionsBuilder.IsConfigured)
      {
        IConfigurationRoot configuration = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json")
           .Build();
        var connectionString = configuration.GetConnectionString("TournamentManagementDatabase");
        optionsBuilder.UseSqlServer(connectionString);
      }
    }
  }
}
