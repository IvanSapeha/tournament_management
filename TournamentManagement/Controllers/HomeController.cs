using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;

namespace TournamentManagement.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFileProvider _fileProvider;

        public HomeController(IFileProvider fileProvider)
        {
            _fileProvider = fileProvider;
        }

        public IActionResult Index()
        {
            var info = _fileProvider.GetFileInfo("dist/index.html");
            return new PhysicalFileResult(info.PhysicalPath, "text/html");
        }
    }
}
