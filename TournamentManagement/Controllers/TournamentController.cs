using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TournamentManagement.EntityFrameworkContext;
using TournamentManagement.Models;

namespace TournamentManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TournamentController : ControllerBase
    {
        private readonly TournamentManagementContext TournamentManagementContext = new TournamentManagementContext();

        public TournamentController(TournamentManagementContext tournamentManagementContext)
        {
          TournamentManagementContext = tournamentManagementContext;
        }

        [HttpGet("[action]")]
        public List<Tournament> GetTournaments()
        {
            TournamentManagementContext context = new TournamentManagementContext();
            var tournamentRepository = new GenericRepository<Tournament>(context);
            List<Tournament> tournaments = tournamentRepository.Get().ToList();
            return tournaments;
        }

        public Tournament GetTournamentById(int id)
        {
            var tournament = TournamentManagementContext.Tournaments.FirstOrDefault(x=>x.Id == id);
            return tournament;
        }
    }
}
